# AAE DSP
by Mateusz Pietrzkowski

_**Educational repository for DSP Architecture laboratory - Advanced Applied Electronic at Wrocław University of Science and Technology**_

## 1. laboratory
Creating
* repository
* simple project with blinking LED for STM32F407G-DISC1 board
* main project with ADC and DMA for STM32F407G-DISC1 board using Analog Discovery 2 device as a wave generator

## 2. laboratory
### Main project in folder DSP_MainProject
ADC configuration
* ADC clock 24MHz
* DMA request, circular buffer
* external trigger for ADC - TIM3 Trigger Out Event
* TIM3 event at 800kHz
* 16x oversampling - final sampling rate = 50kHz

DAC configuration
* Enable buffer, no trigger
* generate signal from ADC after oversampling

DDS
* algorithm to drive DAC - look-up table with sine wave
* TIM2 event at 100kHz
* setup DDS to generate 1kHz, 1.1kHz, 10kHz and 20kHz

Results of measurement in folder Lab3


## 3. laboratory
### Main project in folder DSP_MainProject
Amplitude Modulation (AM)
* carrier wave 5kHz
* modulating signal (from wave generator ~50-200Hz)

Frequency Modulation (FM)
* carrier wave 2kHz
* modulating signal (from wave generator up to 100Hz)


## 4. laboratory
### Main project in folder DSP_MainProject
IIR filter
* low-pass filter, cutoff frequency 1kHz
* band-pass filter, range 1kHz - 3kHz

Possibility of choose right mode using MODE macro.